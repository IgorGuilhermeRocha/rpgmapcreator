interface IProfile{
    id: string;
    name: string;
    description: string;
    image: string;
}

interface IAboutUs{
    profiles: IProfile[];
}
interface ICheckItemList{
    validItems: Array<string>;
    notValidItems: Array<string>;
}
interface IPricesPanel{
    panelItems: Array<IPricesPanelItem>;
}
interface IPricesPanelItem{
    title: string;
    benefits: Array<string>;
    disadvantages: Array<string>;
    price: number;
}
interface IEmailAndPasswordInputs{
    displayConfirmPassword?: boolean;
    flexDirection?: 'column' | 'row';
    width?: string;
}
interface IButton{
    text: string;
    styles?: React.CSSProperties;
    onClick?: any;
}
interface ITab {
    name: string;
    sectionToGo: string;
    toAnotherRoute?: boolean ;
}

interface IHeader {
    title: string;
    tabs: Array<ITab>;
}

interface IGallery {
    cards: IGalleryCard[];
}

interface IGalleryCard {
    imagePath: string;
    description: string;
}

interface IFeature {
    image: any;
    title: string;
    description: string;
}
interface IFeatures {
    title: string;
    features: IFeature[];
}

interface IImageSlider {
    images: string[];
}

export type {
    ICheckItemList,
    IPricesPanelItem,
    IPricesPanel,
    IEmailAndPasswordInputs,
    IButton,
    IFeature,
    IFeatures,
    ITab,
    IHeader,
    IGalleryCard,
    IGallery,
    IImageSlider,
    IProfile,
    IAboutUs
}