import { toast, ToastPosition } from 'react-toastify';


export class ToastsUtils {

    private static DEFAULT_TOAST_POSITION = toast.POSITION.TOP_RIGHT;
    private static DEFAULT_TOAST_AUTO_CLOSE = 3000;

    public static showErrorToast = (message: string, position: ToastPosition = this.DEFAULT_TOAST_POSITION, autoCloseTime: number = this.DEFAULT_TOAST_AUTO_CLOSE) => {
        toast.error(message, { position: position, autoClose: autoCloseTime})
    }

    public static showSuccessToast = (message: string, position: ToastPosition = this.DEFAULT_TOAST_POSITION, autoCloseTime: number = this.DEFAULT_TOAST_AUTO_CLOSE) => {
        toast.success(message, { position: position,  autoClose: autoCloseTime})
    }
}