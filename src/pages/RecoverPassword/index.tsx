import { useNavigate } from "react-router-dom";
import { toast, ToastContainer, ToastPosition } from "react-toastify";
import { Button } from "../../components/Button";
import { ToastsUtils } from "../../utils/toasts";
import { ValidateUtils } from "../../utils/validate";
import "./styles.css";

export const RecoverPassword: React.FC<any> = () => {

  const navigate = useNavigate();

  const sendMail = (): void => {
    const email = document.getElementById("Email-Input") as HTMLInputElement;
    if(ValidateUtils.validEmail(email.value)) { 
      ToastsUtils.showSuccessToast('Um link de recuperação foi enviado para o seu e-mail.', toast.POSITION.TOP_RIGHT, 1000); 
      setTimeout(() => navigate('/loginAndRegistration/true'), 2000);
    }
    else ToastsUtils.showErrorToast('Formato de e-mail inválido.');
  }

  return (
    <div className="Recover-Password-Main-Container">
      <div className="Recover-Password-Form">
        <h1 className="Recover-Password-Title">Esqueci minha senha</h1>
        <p>
          Informe o seu e-mail:
        </p>
        <input type={"email"} id="Email-Input" placeholder="e-mail"/>
        <Button text="Enviar" styles={{marginTop: '10px', width: '100%'}} onClick={sendMail}/>
      </div>
      <ToastContainer />
    </div>
  );
};
