import { ImageSlider } from "../../components/ImageSlider";
import { LoginForm } from "../../components/LoginForm";
import map1 from "../../assets/login-registration/1.jpeg";
import map2 from "../../assets/login-registration/2.jpeg";
import map3 from "../../assets/login-registration/3.jpeg";
import './styles.css'
import { useParams } from "react-router-dom";
import { RegistrationForm } from "../../components/RegistrationForm";

export const LoginAndRegistration: React.FC<any> = () => {
  const { isLogin } = useParams();

  return (
    <div id={"Login-And-Registration"}>
      <div className={"Login-And-Registration-Form"}>
        <ImageSlider images={[map1, map2, map3]} />

        {isLogin === "true" ? <LoginForm /> : <RegistrationForm/> }
      </div>
    </div>
  );
};
