import { IAboutUs } from "../../interfaces";
import { Profile } from "../Profile";
import "./styles.css";
export const AboutUs: React.FC<IAboutUs> = ({ profiles }) => {
  return (
    <div id="About-Us">
      <h2>Sobre nós</h2>

      <div className="About-Us-Profile-Slider-Wrapper">
        <div className="About-Us-Profile-Slider">
          {profiles.map((profile) => (
            <Profile
              id={profile.id}
              name={profile.name}
              description={profile.description}
              image={profile.image}
            />
          ))}
        </div>

        <div className="Slider-Nav">
          {profiles.map((profile) => (
            <a href={`#${profile.id}`} />
          ))}
        </div>
        
      </div>
    </div>
  );
};
