import { useNavigate } from "react-router-dom";
import "./styles.css";
export const ChooseLoginOrRegisterButtons: React.FC = () => {
  const navigate = useNavigate();
  const navigateToAnotherPage = (page: string) => {
   
    navigate(page);
  }
  
  return (
      <div className="Choose-Login-Or-Register">
        <button onClick={() => { navigateToAnotherPage('/loginAndRegistration/true') }}>Entrar</button>
        <button onClick={() => { navigateToAnotherPage('/loginAndRegistration/false') }} >Criar uma conta</button>
      </div>
  );
};
