import "./styles.css";
import { FaDiscord, FaGitlab, FaInstagram, FaLinkedin} from "react-icons/fa";
export const FooterComunity: React.FC<any> = () => {

  const openLinkInNewPage = (link: string) => {
    window.open(link, '_blank');
  };

  return (
    <div id="Comunity">
        <FaGitlab color={"#ffffff"} size={35} style={{cursor: 'pointer'}} onClick={() => openLinkInNewPage('https://gitlab.com/IgorGuilhermeRocha')}/>
        <FaInstagram color={"#ffffff"} size={35} style={{cursor: 'pointer'}} onClick={() => openLinkInNewPage('https://www.instagram.com/igor_guilherme_rocha/')}/>
        <FaLinkedin color={"#ffffff"} size={35} style={{cursor: 'pointer'}}  onClick={() => openLinkInNewPage('https://www.linkedin.com/in/igor-guilherme-945525214/')}/>
        <FaDiscord color={"#ffffff"} size={35} style={{cursor: 'pointer'}} onClick={() => openLinkInNewPage('https://discord.gg/pyP4afw3')}/>
    </div>
  );
};
