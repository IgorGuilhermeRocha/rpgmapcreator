import { useNavigate } from "react-router-dom";
import { ITab } from "../../interfaces";
import "./styles.css";

const Tab: React.FC<ITab> = ({
  name,
  sectionToGo,
  toAnotherRoute,
}): JSX.Element => {
  const navigate = useNavigate();

  function scrollToSection(sectionId: string): void {
    const section = document.querySelector(sectionId);
    if (section) section.scrollIntoView({ behavior: "smooth" });
  }

  return (
    <>
      {toAnotherRoute ? (
        <div className="Tab" onClick={() => navigate(sectionToGo)}>
          <span>{name}</span>
        </div>
      ) : (
        <div className="Tab" onClick={() => scrollToSection(`#${sectionToGo}`)}>
          <span>{name}</span>
        </div>
      )}
    </>
  );
};

export default Tab;
