import { IProfile } from "../../interfaces";
import "./styles.css";
export const Profile: React.FC<IProfile> = ({
  id,
  name,
  description,
  image,
}) => {
  return (
    <div id={id}>
      <div className="Profile-Bundle">
        <img src={image} alt="" />
        <div className="Profile-Name-Description">
          <h3> {name} </h3>
          <span>{description}</span>
        </div>
      </div>
    </div>
  );
};
