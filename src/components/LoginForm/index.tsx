import { useNavigate } from "react-router-dom";
import { ChooseLoginOrRegisterButtons } from "../ChooseLoginOrRegister";
import { EmailAndPasswordInputs } from "../EmailAndPasswordInputs";
import "./styles.css";
export const LoginForm: React.FC = () => {
  const navigate = useNavigate();

  const navigateToPage = (page: string) => {
    navigate(page);
  }

  return (
    <div id="Login-Main-Container" style={{ width: "50%" }}>
      <ChooseLoginOrRegisterButtons/>

      <div className={"Login-Form"}>
        <EmailAndPasswordInputs/>


        <button>Entrar</button>

        <div className={"Login-Form-Row"}>
          <a onClick={ () => navigateToPage('/loginAndRegistration/false') }>Não tenho uma conta</a>
          <a onClick={ () => navigateToPage('/recoverPassword') }>Esqueci minha senha</a>
        </div>
      </div>
    </div>
  );
};
