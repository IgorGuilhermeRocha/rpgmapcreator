import { IPricesPanel } from "../../interfaces";
import { PricesPanelItem } from "../PricesPanelItem";
import "./styles.css";
export const PricesPanel: React.FC<IPricesPanel> = ({ panelItems }) => {
  return (
    <div id="Prices-Main-Panel">
      <h2> Nosso planos </h2>
      <div id="Prices-Item-Container">
      {panelItems.map((item) => (
        <PricesPanelItem
          title={item.title}
          benefits={item.benefits}
          disadvantages={item.disadvantages}
          price={item.price}
        />
      ))}
      </div>
    </div>
  );
};
