import "./styles.css";
export const NameAndLastNameInputs: React.FC = () => {
  return (
    <div className="Name-And-Last-Name-Inputs-Container">
      <input type={'text'} className="Register-Name-Input" placeholder="Nome" />
      <input type={'text'} className="Register-Last-Name-Input" placeholder="Sobrenome" />
    </div>
  );
};
