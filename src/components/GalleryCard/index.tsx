import { IGalleryCard } from '../../interfaces';
import './styles.css'

const GalleryCard: React.FC<IGalleryCard> = ({ imagePath, description }): JSX.Element => {
  return (
    <div className='Gallery-Card'>
      <img src={imagePath} className='Gallery-Card-Image' />
      <span>{description}</span>
    </div>
  );
};

export default GalleryCard;
