import { IPricesPanelItem } from "../../interfaces";
import { CheckItemList } from "../CheckItemList";
import "./styles.css";
export const PricesPanelItem: React.FC<IPricesPanelItem> = ({title, benefits, disadvantages, price}) => {
  return (
    <div className="Prices-Item">
      <h3>{title}</h3>
      <h4>R${price},00</h4>
      { <CheckItemList validItems={benefits} notValidItems={disadvantages}/> }
    </div>
  );
};
