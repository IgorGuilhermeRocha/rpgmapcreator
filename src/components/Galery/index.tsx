import { IGallery } from '../../interfaces';
import GalleryCard from '../GalleryCard';
import './styles.css'

const Gallery: React.FC<IGallery> = ({ cards }): JSX.Element => {
  return (
    <div id="Gallery">

      <div className="Gallery-Cards">
        {cards.map((item, index) => (
          <GalleryCard imagePath={item.imagePath} description={item.description} />
        ))}
      </div>
    </div>
  );
};

export default Gallery;
