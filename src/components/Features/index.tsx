import { IFeatures } from "../../interfaces"
import Feature from "../Feature"
import './styles.css'

const Features: React.FC<IFeatures> = ({ title, features }): JSX.Element => {
    return (
        <div id="Features">
            <h2>{title}</h2>
            <div className="Gallery-Cards">
                {features.map((item, index) => (
                    <Feature image={item.image} title={item.title} description={item.description} />
                ))}
            </div>

        </div>

    )

}

export default Features