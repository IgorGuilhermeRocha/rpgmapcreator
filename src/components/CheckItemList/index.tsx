import { ICheckItemList } from "../../interfaces";
import "./styles.css";
export const CheckItemList: React.FC<ICheckItemList> = ({validItems, notValidItems}) => {
  return (
    <div id="Check-Item-List-Main-Container">
      <ul>
      
        {validItems.map((item) => 
          <li className="Valid-Item">{item}</li>
        )}
    
  
        {notValidItems.map((item) => 
          <li className="Not-Valid-Item">{item}</li>
        )}
  
      </ul>
    </div>
  );
};
