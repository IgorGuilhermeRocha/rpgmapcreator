import { IFeature } from "../../interfaces";
import "./styles.css";

const Feature: React.FC<IFeature> = ({
  title,
  description,
  image,
}): JSX.Element => {
  return (
    <div className="Feature">
        {image}
        <h3>{title}</h3>
        <span>{description}</span>

    </div>
  );
};

export default Feature;
