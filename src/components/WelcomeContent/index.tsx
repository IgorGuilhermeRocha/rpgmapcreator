import "./styles.css";
import map from "../../assets/map2.png";
import FreeAndProVersionButtons from "../FreeAndProVersionButton";

const WelcomeContent: React.FC<any> = ({ }): JSX.Element => {
  return (
    <div className="Welcome-Content">
      <div className="Welcome-Content-Text">
        <h1 className="Welcome-Content-Text-Title">
          Crie seus mapas rpg com BinhoCreator
        </h1>
        <span className="Welcome-Content-Text-Description">
          Torne suas ideias em realidade, BinhoCreator é a maior plataforma de
          criação de mapas online do mundo. <br /> <br /> <br /> Ideal
          para jogadores e entusiastas de RPG.
        </span>

        <FreeAndProVersionButtons />
      </div>

      <img src={map}></img>
    </div>
  );
};

export default WelcomeContent;
