import { useEffect, useState } from "react";
import { IImageSlider } from "../../interfaces";
import "./styles.css"

export const ImageSlider: React.FC<IImageSlider> = ({ images }) => {
  const [showedImage, setShowedImage] = useState<string>(images[0]);
  const [count, setCount] = useState<number>(1);

  useEffect(() => {
    const interval = setInterval(() => {
      setCount((prevCount) => {
        const aux = prevCount + 1;
        return aux > images.length - 1 ? 0 : aux;
      });
      setShowedImage(images[count]);
    }, 5000);

    return () => {
      clearInterval(interval);
    };
  }, [showedImage]);

  return (
    <>
      <div className="Image-Slider">
        <img src={showedImage} alt="" />
      </div>
    </>
  );
};
