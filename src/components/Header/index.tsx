import { IHeader } from '../../interfaces';
import Tab from '../Tabs';
import './styles.css'

const Header: React.FC<IHeader> = ({title, tabs}): JSX.Element => {
    return (
       <header className='Header'>
            <div className='Header-Title'>
                <span>{title}</span>
            </div>

            <div className='Header-Title-Separator'/>

            <div className='Header-Tabs'>
                {tabs.map((item, index) => (
                    <Tab name={item.name} sectionToGo={item.sectionToGo}/>
                ))}
            </div>

            <div className='Header-Tabs-Registration'>
                <Tab name={'Entrar'} sectionToGo={'/loginAndRegistration/true'} toAnotherRoute={true}/>
                <Tab name={'Criar uma conta'} sectionToGo={'/loginAndRegistration/false'} toAnotherRoute={true}/>
            </div>
       </header>
    )
}


export default Header;