import { IButton } from "../../interfaces";
import "./styles.css";

export const Button: React.FC<IButton> = ({text, styles, onClick}) => {

  return (
      <button id="Basic-Button" style={styles} onClick={onClick}>{text}</button>
  );
};
