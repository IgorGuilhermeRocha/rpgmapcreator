import "./styles.css";

const FreeAndProVersionButtons: React.FC<any> = ({}): JSX.Element => {
    
  function scrollToSection(sectionId: string): void {
    const section = document.querySelector(sectionId);
    if (section) section.scrollIntoView({ behavior: "smooth" });
  }

  return (
    <div className="Buttons-Container">
      <button className="Default-Button" onClick={() => scrollToSection('#Features')}>
        Experimente a versão gratuita
      </button>
      <span>ou</span>
      <button className="Default-Button" onClick={() => scrollToSection('#Features')} > Veja a versão paga</button>
    </div>
  );
};

export default FreeAndProVersionButtons;
