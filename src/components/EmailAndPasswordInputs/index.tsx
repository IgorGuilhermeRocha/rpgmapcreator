import { IEmailAndPasswordInputs } from "../../interfaces";
import "./styles.css";
export const EmailAndPasswordInputs: React.FC<IEmailAndPasswordInputs> = ({
  displayConfirmPassword = false,
  flexDirection = 'column',
  width = '90%'
}) => {
  return (
      <div className="Email-And-Password-Inputs" style={{flexDirection: flexDirection}} >
        <input
          type="email"
          placeholder="Digite seu e-mail"
          id="Form-Email-Input"
          style={{width: width}}
        />

        <input
          type="password"
          placeholder="Digite sua senha"
          id="Form-Password-Input"
          style={{width: width}}
        />
         {displayConfirmPassword && (
        <input id="Form-Confirm-Password-Input" type="password" placeholder="Confirme sua senha" style={{width: '90%'}}/>
      )}
      </div>
  );
};
