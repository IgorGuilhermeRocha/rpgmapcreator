import { Button } from "../Button";
import { ChooseLoginOrRegisterButtons } from "../ChooseLoginOrRegister";
import { EmailAndPasswordInputs } from "../EmailAndPasswordInputs";
import { NameAndLastNameInputs } from "../NameAndLastNameInputs";
import "./styles.css";

export const RegistrationForm: React.FC = () => {


  return (
    <div id="Register-Main-Container" style={{ width: "50%" }}>
      <ChooseLoginOrRegisterButtons/>
   
      <div className={"Register-Form"}>
        <NameAndLastNameInputs/>
        <EmailAndPasswordInputs displayConfirmPassword/>
      </div>

      <div style={{display: 'flex', justifyContent: 'center'}}>
      <Button text="Criar Conta" styles={{width: '93%'}}/>
      </div>
      
    </div>
  );
};
