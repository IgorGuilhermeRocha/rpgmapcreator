import "./App.css";
import Gallery from "./components/Galery";
import Header from "./components/Header";
import WelcomeContent from "./components/WelcomeContent";
import map1 from "../src/assets/gallery-1.jpg";
import map2 from "../src/assets/gallery-2.jpeg";
import map3 from "../src/assets/gallery-3.jpeg";
import map4 from "../src/assets/gallery-4.jpg";
import map5 from "../src/assets/gallery-5.jpg";
import icon1 from "../src/assets/foguete.png";
import IgorGuilherme from "../src/assets/about-us/igor-is-back.jpg";
import MatheusNunes from "../src/assets/about-us/Matheus-Nunes.jpeg";
import Gabriel from "../src/assets/about-us/Gabriel.jpg";
import Junnin from "../src/assets/about-us/Junnin.jpg";
import Lucas from "../src/assets/about-us/Lucas.jpg"
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


import Features from "./components/Features";
import { IFeature, IPricesPanelItem, IProfile, ITab } from "./interfaces";
import { PricesPanel } from "./components/Prices";
import { AboutUs } from "./components/AboutUs";
import { FooterComunity } from "./components/FooterComunity";
import { FaConciergeBell, FaDiscord, FaHeart, FaRobot } from "react-icons/fa";

function App() {
  const ITabs: ITab[] = [
    { name: "Galeria", sectionToGo: "Gallery" },
    { name: "Funcionalidades", sectionToGo: "Features" },
    { name: "Preços", sectionToGo: "Prices-Main-Panel" },
    { name: "Sobre nós", sectionToGo: "About-Us" },
    { name: "Comunidade", sectionToGo: "Comunity" },
  ];

  const galleryCards = [
    {
      imagePath: map1,
      description:
        "A floresta é exuberante e exala uma atmosfera serena. Árvores imponentes criam uma cobertura densa, filtrando a luz solar. Os personagens podem seguir trilhas estreitas, atravessar riachos cristalinos e encontrar clareiras pacíficas. No entanto, a floresta também abriga perigos, como emboscadas de bestas selvagens, teias de aranhas gigantes e até mesmo uma clareira assombrada por espíritos vingativos.",
    },
    {
      imagePath: map2,
      description:
        "Um Battle Map de Templo exibe uma estrutura antiga e imponente, com paredes de pedra decoradas com entalhes intricados. No pátio central, altares dedicados a divindades circundam os aventureiros. Corredores sombrios conduzem a salas com relíquias sagradas, bibliotecas de pergaminhos antigos e câmaras secretas. Uma câmara principal revela um altar imponente, enquanto uma aura divina paira no ar. Os desafios incluem armadilhas, guardiões espirituais e cultistas.",
    },
    {
      imagePath: map3,
      description:
        "A caverna é um labirinto escuro e sinistro, com paredes de rocha áspera e caminhos tortuosos. Estalactites e estalagmites adornam o ambiente, criando obstáculos naturais. Existem câmaras ocultas com tesouros escondidos, poços de lava borbulhante e pontes precárias. Criaturas subterrâneas como morcegos gigantes e trolls podem ser encontradas nas profundezas da caverna, proporcionando combates desafiadores.",
    },
    {
      imagePath: map4,
      description:
        "A cidade é composta por ruas estreitas e sinuosas, com prédios altos e variados ao redor. Os personagens podem explorar becos sombrios, praças movimentadas e mercados agitados. Há pontos de interesse, como uma taverna lotada, uma praça de execução e uma torre de observação. Os jogadores devem lidar com multidões, negociar com mercadores e talvez até enfrentar encontros clandestinos nas sombras da cidade.",
    },
    {
      imagePath: map5,
      description:
        "A caverna é um labirinto escuro e úmido, com formações rochosas impressionantes e estalactites pendendo do teto. O caminho principal é estreito, mas os personagens podem descobrir passagens secretas e fissuras estreitas para explorar. Existem lagos subterrâneos com águas profundas e correntezas perigosas. A caverna abriga criaturas temíveis, como trolls, goblins e até mesmo um dragão adormecido em seu covil.",
    },
  ];

  const features: IFeature[] = [
    {
      image: <FaHeart size={30} color={"#34aeeb"}/>,
      title: "Interface amigável",
      description: "Interface projetada com foco na experiência de usuário, intuitiva e fácil de usar, permitindo que os usuários façam seus mapas de forma eficiente e sem complicações.",
    },
    {
      image: <FaConciergeBell size={30} color={"#34aeeb"}/>,
      title: "Atualizações constantes e novas funcionalidades",
      description: "Sempre estaremos atualizando nosso software, para garantir a satisfação dos nosso clientes.",
    },
    {
      image: <FaRobot size={30} color={"#34aeeb"}/>,
      title: "Suporte de IA",
      description: "Apoio de uma inteligência artificial que ajudar no desenvolvimento dos mapas.",
    },
    {
      image: <FaDiscord size={30} color={"#34aeeb"}/>,
      title: "Comunidade Ativa",
      description: "Comunidade ativa para novas amizades e tirar dúvidas.",
    },
  ];

  const pricesPanels: Array<IPricesPanelItem> = [
    {
      title: "Plano Gratuito",
      benefits: ["Acesso a ferramenta de desenho de mapas"],
      disadvantages: [
        "Uso ilimitado da nossa Inteligência Artificial",
        "Acesso a mapas feitos por outras pessoas",
        "Acesso a ferramentas exclusivas",
      ],
      price: 0,
    },

    {
      title: "Plano Básico",
      benefits: [
        "Acesso a ferramenta de desenho de mapas",
        "Acesso a mapas feitos por outras pessoas",
        "Acesso a ferramentas exclusivas",
      ],
      disadvantages: ["Uso ilimitado da nossa Inteligência Artificial"],
      price: 15,
    },

    {
      title: "Plano Profissional",
      benefits: [
        "Acesso a ferramenta de desenho de mapas",
        "Acesso a mapas feitos por outras pessoas",
        "Acesso a ferramentas exclusivas",
        "Uso ilimitado da nossa Inteligência Artificial",
      ],
      disadvantages: [],
      price: 50,
    },
  ];

  const profiles: Array<IProfile> = [
    {
      id: "Profile-1",
      name: "Gabriel Oliveira Gomes",
      description: "O idealizador do projeto, responsável por identificar o tema principal e suas nuances, colocou as ideias no papel. Com uma visão clara e perspicaz, Gabriel deu vida às concepções iniciais. Seu papel crucial foi transformar a inspiração em realidade, com habilidade e dedicação, ele definiu os contornos e direcionamentos, proporcionando um ponto de partida para o sucesso do projeto.",
      image: Gabriel,
    },
    {
      id: "Profile-2",
      name: "Igor Guilherme Almeida Rocha",
      description: "Desenvolvedor principal do projeto, foi o responsável por conduzir todas as etapas do desenvolvimento, desde a criação do back-end até a implementação do front-end.Com habilidades técnicas especializadas e vasta experiência em programação, ele desempenhou um papel crucial na transformação das ideias em um produto finalizado.",
      image: IgorGuilherme,
    },
    {
      id: "Profile-3",
      name: "Matheus Nunes",
      description: "Profissional de QA altamente capacitado, o responsável pelos testes de API, testes unitários e testes de integração desempenhou um papel crítico na garantia da qualidade do projeto. Utilizando metodologias rigorosas e ferramentas avançadas, ele realizou testes minuciosos para identificar e corrigir quaisquer problemas ou falhas no sistema.",
      image: MatheusNunes,
    },
    {
      id: "Profile-4",
      name: "Alexsandro Junior" ,
      description: "Ele foi parte fundamental nas fases anteriores do projeto, desempenhando um papel crucial na pesquisa do tema e na definição das características dos clientes e personas. Sua participação nessas etapas iniciais foi essencial para estabelecer uma base sólida e fundamentada para o desenvolvimento do projeto como um todo.",
      image: Junnin,
    },
    {
      id: "Profile-5",
      name: "Lucas Gabriel",
      description: "Ele foi parte fundamental no desenvolvimento do projeto como um todo, atuando não apenas nos testes, mas também em diversas outras etapas. Sua contribuição foi essencial para o sucesso do projeto, pois ele demonstrou um profundo conhecimento e habilidades técnicas excepcionais. Sua capacidade de resolver problemas complexos e lidar com desafios inesperados foi crucial para superar obstáculos ao longo do caminho.",
      image: Lucas,
    },
  ];

  return (
    <div className="App-main-container">
      <Header title="BinhoCreator" tabs={ITabs} />
      <WelcomeContent />
      <Gallery cards={galleryCards} />
      <Features
        title={"Ferramenta para criação de mapas online"}
        features={features}
      />
      <PricesPanel panelItems={pricesPanels} />
      <AboutUs profiles={profiles} />
      <FooterComunity/>
      <ToastContainer />
    </div>
  );
}

export default App;
